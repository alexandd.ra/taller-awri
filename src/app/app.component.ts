import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  show: boolean = true;
  nombres: string[] = [];


  campo ="";
  nombre = "";

  addNombre(): void {
    this.nombres.push(this.nombre);
  }

}
